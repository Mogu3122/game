{
    "id": "2a4cbd0f-aee5-48e6-8fe8-b7cc2dc13688",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "debccf5c-4f7f-4264-beb0-be9ce63979dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a4cbd0f-aee5-48e6-8fe8-b7cc2dc13688",
            "compositeImage": {
                "id": "a8cff724-928b-4797-a650-9c20376c9a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "debccf5c-4f7f-4264-beb0-be9ce63979dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c66630-0f38-4d5f-966a-833b13ebd59b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "debccf5c-4f7f-4264-beb0-be9ce63979dc",
                    "LayerId": "8a32db20-20d9-48b7-a046-feb1c4ec1eae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8a32db20-20d9-48b7-a046-feb1c4ec1eae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a4cbd0f-aee5-48e6-8fe8-b7cc2dc13688",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 36,
    "yorig": 42
}